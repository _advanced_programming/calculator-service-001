package ap.calculator.app.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ap.calculator.app.IntegerCalculatorService;

public class IntegerCalculatorServiceTest {
	
	@Test
	public void testAdd() {
		IntegerCalculatorService service = new IntegerCalculatorService();
		int num1 =4, num2 = 5;
		int result = service.add(num1, num2);
		
		assertEquals(num1+num2,result);
	}

}
