package ap.calculator.app;

import ap.calculator.lib.ICalculatorService;

public class IntegerCalculatorService implements ICalculatorService<Integer> {

	public Integer add(Integer item1, Integer item2) {
		
		return item1 + item2;
	}

}
