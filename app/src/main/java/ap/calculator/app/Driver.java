package ap.calculator.app;

import ap.calculator.lib.ICalculatorService;

public class Driver {

	public static void main(String[] args) {
		ICalculatorService<Integer> service = new IntegerCalculatorService();
		int num1=5,num2=4;
		System.out.println(
				String.format("%d + %d = %d", num1,num2,service.add(num1, num2))
				);

	}

}
