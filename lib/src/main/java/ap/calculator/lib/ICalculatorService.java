package ap.calculator.lib;

/**
 * Performs calculations on items of type T
 * */
public interface ICalculatorService<T> {
	/**
	 * Adds two items
	 * @param item1 - first item to add
	 * @param item2 - second item to add
	 * @return sum of two items
	 * */
   T add(T item1, T item2);
}
