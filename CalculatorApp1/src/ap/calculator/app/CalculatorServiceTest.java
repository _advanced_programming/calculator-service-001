package ap.calculator.app;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculatorServiceTest {

	@Test
	public void testAdd() {
		int num1 =4, num2 = 5;
		CalculatorService service = new CalculatorService();
		int result = service.add(num1, num2);
		assertEquals(num1 + num2,result);
	}

}
