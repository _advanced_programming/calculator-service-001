package ap.calculator.app;

import ap.calculator.lib.ICalculatorService;

public class Driver {
	
	public static void main(String[] args) {
		ICalculatorService service = new CalculatorService();
		System.out.println(
				String.format("%d + %d = %d", 5,7,service.add(5, 7))
				);
		
	}

}
