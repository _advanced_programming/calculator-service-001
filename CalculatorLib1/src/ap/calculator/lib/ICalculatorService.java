package ap.calculator.lib;

/**
 * This service will assist us in performing calculations
 * */
public interface ICalculatorService {
	/**
	 * <h1>Our Add Method</h1>
	 * Adds two numbers
	 * @param num1 - first number to add
	 * @param num2 - second number to add
	 * @return the sum of two numbers
	 * */
	int add(int num1, int num2);

}
